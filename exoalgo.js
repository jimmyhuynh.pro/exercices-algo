// Exercice 1
// Ecrire un algorithme qui demande à l’utilisateur un nombre compris entre 1 et 3 jusqu’à ce que la réponse convienne.

// Var n(entier), b
// b-> 0
// Tant que b est égal 0
// Afficher "Veuillez saisir un nombre"
// Saisir un nombre
// Si n<1 ou n>3 Alors Afficher "continue"
// Si b->1 Alors Afficher "bien joué"

let b = 0
while (b == 0) {
    let n = parseInt(prompt("Veuillez saisir un nombre"));
    if (n < 1 || n > 3) {
        alert("continue")
    }
    else {
        alert("bien joué")
        b = 1
    }
}

// Exercice 2
// Ecrire un algorithme qui demande un nombre compris entre 10 et 20, jusqu’à ce que la réponse convienne. En cas de réponse supérieure à 20, on fera apparaître un message : « Plus petit ! », et inversement, « Plus grand ! » si le nombre est inférieur à 10.

// Var n(entier), b
// b->0
// Tant que b est égal à 0
// Demander un nombre entier
// Saisir un nombre
// Si n>20 Alors Afficher "Plus petit !"
// Si n<10 Alors Afficher "Plus grand!"
// Si b->1 Alors Afficher "ggwp"

let b = 0
while (b == 0) {
    let n = parseInt(prompt("Un nombre entier"));
    if (n > 20)
        alert("Plus petit !")
    else if (n < 10)
        alert("Plus grand !")
    else {
        alert("ggwp")
        b = 1
    }
}

// Exercice 3
// Ecrire un algorithme qui demande un nombre de départ, et qui ensuite affiche les dix nombres suivants. Par exemple, si l'utilisateur entre le nombre 17, le programme affichera les nombres de 18 à 27.

// Var n(entier), a
// n->Afficher "Ecris un nombre"
// Saisir un nombre
// a-> n+10
// Pour n,tant que n inférieur à a, incrémenter
// Afficher n+1

let n = parseInt(prompt("Ecris un nombre"));
let a = n + 10;
for (n; n < a; n++) {
    console.log(n + 1)
}

// Exercice 4
// Réécrire l'algorithme précédent, en utilisant cette fois l'instruction Pour (Si ce n'est pas déjà le cas)

// Exercice 5
// Ecrire un algorithme qui demande un nombre de départ, et qui ensuite écrit la table de multiplication de ce nombre, présentée comme suit (cas où l'utilisateur entre le nombre 7) : Table de 7 : 7 x 1 = 7 7 x 2 = 14 7 x 3 = 21 ... 7 x 10 = 70

// Var n(entier), a, b
// n Afficher "Ecris un nombre entier"
// Saisir un nombre
// a -> 1
// Pour a,tant que a inférieur à 11, incrémenter
// b-> n*a
// Afficher n+a+b

let n = parseInt(prompt("Ecris un nombre entier"));
let a = 1;
for (a; a < 11; a++) {
    let b = n * a
    console.log(n + "x" + a + "=" + b)
}

// Exercice 6
// Ecrivez un algorithme calculant la somme des valeurs d’un tableau (on suppose que le tableau a été préalablement saisi).

// Var array, sum, i
// sum -> 0
// Pour i-> 0, tant que i inférieur à la longueur de l'array, incrémenter
// sum -> sum + array de i
// Afficher sum

let array = [1, 2, 3, 4, 5]
let sum = 0;
for (let i = 0; i < array.length; i++) {
    sum += array[i];
}
console.log(sum);

// Exercice 7
// Ecrire un algorithme qui demande successivement 20 nombres à l’utilisateur, et qui lui dise ensuite quel était le plus grand parmi ces 20 nombres : Entrez le nombre numéro 1 : 12 Entrez le nombre numéro 2 : 14 etc. Entrez le nombre numéro 20 : 6 Le plus grand de ces nombres est : 14 Modifiez ensuite l’algorithme pour que le programme affiche de surcroît en quelle position avait été saisie ce nombre : C’était le nombre numéro 2

// Var a(tableau), i
// a -> tableau
// Pour i-> 0, tant que i inférieur à 20, incrémenter
// Afficher " Entrez un nombre" 
// Saisir un nombre 20 fois
// Afficher "Le nombre le plus grand est" 
// Afficher "C'était le nombre numéro"

let a = []
for (i = 0; i < 20; i++) {
    a[i] = parseInt(prompt("Entrez un nombre"));
}
alert("Le nombre le plus grand est" + " " + Math.max(...a) + "." + "C'était le nombre numéro" + " " + a.indexOf(Math.max(...a)));


// Exercice 8
// Réécrire l’algorithme précédent, mais cette fois-ci on ne connaît pas d’avance combien l’utilisateur souhaite saisir de nombres. La saisie des nombres s’arrête lorsque l’utilisateur entre un zéro.

// Var a(tableau), p, nbr, boolean
// Function user
// a -> tableau
// boolean ->false
// Tant que boolean strictement false
// p -> Affiche " Entrez un nombre"
// nbr -> Renvoie un entier
// Envoyer nbr dans le tableau a
// Si nbr est égal à 0
// boolean -> true
// Afficher "Le nombre le plus grand est" 
// Afficher "C'était le nombre numéro"
// return

function user() {
    let a = []
    let boolean = false

    while (boolean === false) {
        let p = prompt("Entrez un nombre")
        let nbr = parseInt(p)
        a.push(nbr);
        if (nbr == 0) {
            boolean = true
            alert("Le nombre le plus grand est" + " " + Math.max(...a) + "." + "C'était le nombre numéro" + " " + a.indexOf(Math.max(...a)));
            return
        }
    }
}
console.log(user())

// Exercice 9
// Lire la suite des prix (en euros entiers et terminée par zéro) des achats d’un client. Calculer la somme qu’il doit, lire la somme qu’il paye, et simuler la remise de la monnaie en affichant les textes "10 Euros", "5 Euros" et "1 Euro" autant de fois qu’il y a de coupures de chaque sorte à rendre.

// //Var a(tableau), sum, paye, somme, remise, boolean, argent10, argent5, argent1
// funtion achat
// a -> tableau
// boolean -> false
// argent10 -> 0
// argent5 -> 0
// argent1 -> 0
// Tant que boolean est strictement égal à false
// sum Afficher "Achats client"
// Saisir nombre
// Envoyer sum dans le tableau a
// Si la sum est égal à 0
// boolean -> true
// sum -> somme des valeurs du tableau a
// Afficher "La somme de vos achats est de" sum
// paye -> valeur saisi
// remise -> paye - somme
// Si paye > somme
// Afficher"Je dois vous rendre" + remise
// Tant que la remise est >=10
// remise -= 10
// argent10 incrémenter
// Tant que la remise est >=5
// remise -= 5
// argent5 incrémenter
// Tant que la remise est >=1
// remise -=1
// argent1 incrémenter
// Afficher "Je vous ai rendu "+argent10+" billet de 10,"+argent5+" billet de 5 et "+argent1+" pièce de 1."
// Sinon Afficher "Vous me devez"

function achat() {
    let a = []
    let boolean = false
    let argent10 = 0
    let argent5 = 0
    let argent1 = 0

    while (boolean === false) {
        let sum = parseInt(prompt("Achats client"))
        a.push(sum);
        if (sum == 0) {
            boolean = true
            let sum = (previousValue, currentValue) => previousValue + currentValue
            alert("La somme de vos achats est de " + a.reduce(sum))
            let paye = parseInt(prompt("Vous avez payé"))
            let somme = a.reduce(sum)
            let remise = paye - somme

            if (paye > somme) {
                alert("Je dois vous rendre" + " " + remise)
                while (remise >= 10) {
                    remise -= 10
                    argent10++
                }
                while (remise >= 5) {
                    remise -= 5
                    argent5++
                }
                while (remise >= 1) {
                    remise -= 1
                    argent1++
                }

                alert("Je vous ai rendu " + argent10 + " billet de 10, " + argent5 + " billet de 5 et " + argent1 + " pièce de 1.")

            } else {
                alert("Vous me devez" + " " + remise * -1)
            }
        }
    }

}

console.log(achat())